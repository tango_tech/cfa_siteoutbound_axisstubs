/**
 * SyncSiteRequestABM.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abm.v1;

public class SyncSiteRequestABM  implements java.io.Serializable {
    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.UserHeaderABOType userHeader;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.SiteABOType request;

    public SyncSiteRequestABM() {
    }

    public SyncSiteRequestABM(
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.UserHeaderABOType userHeader,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.SiteABOType request) {
           this.userHeader = userHeader;
           this.request = request;
    }


    /**
     * Gets the userHeader value for this SyncSiteRequestABM.
     * 
     * @return userHeader
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.UserHeaderABOType getUserHeader() {
        return userHeader;
    }


    /**
     * Sets the userHeader value for this SyncSiteRequestABM.
     * 
     * @param userHeader
     */
    public void setUserHeader(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.UserHeaderABOType userHeader) {
        this.userHeader = userHeader;
    }


    /**
     * Gets the request value for this SyncSiteRequestABM.
     * 
     * @return request
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.SiteABOType getRequest() {
        return request;
    }


    /**
     * Sets the request value for this SyncSiteRequestABM.
     * 
     * @param request
     */
    public void setRequest(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.SiteABOType request) {
        this.request = request;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SyncSiteRequestABM)) return false;
        SyncSiteRequestABM other = (SyncSiteRequestABM) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userHeader==null && other.getUserHeader()==null) || 
             (this.userHeader!=null &&
              this.userHeader.equals(other.getUserHeader()))) &&
            ((this.request==null && other.getRequest()==null) || 
             (this.request!=null &&
              this.request.equals(other.getRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserHeader() != null) {
            _hashCode += getUserHeader().hashCode();
        }
        if (getRequest() != null) {
            _hashCode += getRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SyncSiteRequestABM.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abm/v1", ">syncSiteRequestABM"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abm/v1", "userHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "UserHeaderABOType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("request");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abm/v1", "request"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "SiteABOType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
