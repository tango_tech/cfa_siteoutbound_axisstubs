/**
 * SiteABOType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class SiteABOType  implements java.io.Serializable {
    private java.lang.String cstSiteIdTX;

    private java.lang.String messageIdNU;

    private java.lang.String cstSiteStatusCL;

    private java.lang.String cstPreviousSiteStatusCL;

    private java.lang.String cstNameTX;

    private java.lang.String cstConceptCodeLI;

    private java.lang.String cstLocationTypeLI;

    private java.lang.String cstLocationNumberTX;

    private java.lang.String triCityTX;

    private java.lang.String triStateProvTX;

    private java.lang.String triLatitudeTX;

    private java.lang.String triLongitudeTX;

    private java.math.BigDecimal cstLandSizeSqftNU;

    private java.math.BigDecimal cstLandSizeAcresNU;

    private java.lang.String cstVaporIntrusionLI;

    private java.lang.String cstMitigationSystemOtherTX;

    private java.lang.String cstMitigationSystemBL;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.StoreType[] closestExistingStore;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.TaskType[] task;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] checklist;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ContractType[] contract;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] contacts;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ResearchForecastsType researchForecasts;

    private java.lang.String cstLegalNotesNO;

    public SiteABOType() {
    }

    public SiteABOType(
           java.lang.String cstSiteIdTX,
           java.lang.String messageIdNU,
           java.lang.String cstSiteStatusCL,
           java.lang.String cstPreviousSiteStatusCL,
           java.lang.String cstNameTX,
           java.lang.String cstConceptCodeLI,
           java.lang.String cstLocationTypeLI,
           java.lang.String cstLocationNumberTX,
           java.lang.String triCityTX,
           java.lang.String triStateProvTX,
           java.lang.String triLatitudeTX,
           java.lang.String triLongitudeTX,
           java.math.BigDecimal cstLandSizeSqftNU,
           java.math.BigDecimal cstLandSizeAcresNU,
           java.lang.String cstVaporIntrusionLI,
           java.lang.String cstMitigationSystemOtherTX,
           java.lang.String cstMitigationSystemBL,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.StoreType[] closestExistingStore,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.TaskType[] task,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] checklist,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ContractType[] contract,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] contacts,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ResearchForecastsType researchForecasts,
           java.lang.String cstLegalNotesNO) {
           this.cstSiteIdTX = cstSiteIdTX;
           this.messageIdNU = messageIdNU;
           this.cstSiteStatusCL = cstSiteStatusCL;
           this.cstPreviousSiteStatusCL = cstPreviousSiteStatusCL;
           this.cstNameTX = cstNameTX;
           this.cstConceptCodeLI = cstConceptCodeLI;
           this.cstLocationTypeLI = cstLocationTypeLI;
           this.cstLocationNumberTX = cstLocationNumberTX;
           this.triCityTX = triCityTX;
           this.triStateProvTX = triStateProvTX;
           this.triLatitudeTX = triLatitudeTX;
           this.triLongitudeTX = triLongitudeTX;
           this.cstLandSizeSqftNU = cstLandSizeSqftNU;
           this.cstLandSizeAcresNU = cstLandSizeAcresNU;
           this.cstVaporIntrusionLI = cstVaporIntrusionLI;
           this.cstMitigationSystemOtherTX = cstMitigationSystemOtherTX;
           this.cstMitigationSystemBL = cstMitigationSystemBL;
           this.closestExistingStore = closestExistingStore;
           this.task = task;
           this.checklist = checklist;
           this.contract = contract;
           this.contacts = contacts;
           this.researchForecasts = researchForecasts;
           this.cstLegalNotesNO = cstLegalNotesNO;
    }


    /**
     * Gets the cstSiteIdTX value for this SiteABOType.
     * 
     * @return cstSiteIdTX
     */
    public java.lang.String getCstSiteIdTX() {
        return cstSiteIdTX;
    }


    /**
     * Sets the cstSiteIdTX value for this SiteABOType.
     * 
     * @param cstSiteIdTX
     */
    public void setCstSiteIdTX(java.lang.String cstSiteIdTX) {
        this.cstSiteIdTX = cstSiteIdTX;
    }


    /**
     * Gets the messageIdNU value for this SiteABOType.
     * 
     * @return messageIdNU
     */
    public java.lang.String getMessageIdNU() {
        return messageIdNU;
    }


    /**
     * Sets the messageIdNU value for this SiteABOType.
     * 
     * @param messageIdNU
     */
    public void setMessageIdNU(java.lang.String messageIdNU) {
        this.messageIdNU = messageIdNU;
    }


    /**
     * Gets the cstSiteStatusCL value for this SiteABOType.
     * 
     * @return cstSiteStatusCL
     */
    public java.lang.String getCstSiteStatusCL() {
        return cstSiteStatusCL;
    }


    /**
     * Sets the cstSiteStatusCL value for this SiteABOType.
     * 
     * @param cstSiteStatusCL
     */
    public void setCstSiteStatusCL(java.lang.String cstSiteStatusCL) {
        this.cstSiteStatusCL = cstSiteStatusCL;
    }


    /**
     * Gets the cstPreviousSiteStatusCL value for this SiteABOType.
     * 
     * @return cstPreviousSiteStatusCL
     */
    public java.lang.String getCstPreviousSiteStatusCL() {
        return cstPreviousSiteStatusCL;
    }


    /**
     * Sets the cstPreviousSiteStatusCL value for this SiteABOType.
     * 
     * @param cstPreviousSiteStatusCL
     */
    public void setCstPreviousSiteStatusCL(java.lang.String cstPreviousSiteStatusCL) {
        this.cstPreviousSiteStatusCL = cstPreviousSiteStatusCL;
    }


    /**
     * Gets the cstNameTX value for this SiteABOType.
     * 
     * @return cstNameTX
     */
    public java.lang.String getCstNameTX() {
        return cstNameTX;
    }


    /**
     * Sets the cstNameTX value for this SiteABOType.
     * 
     * @param cstNameTX
     */
    public void setCstNameTX(java.lang.String cstNameTX) {
        this.cstNameTX = cstNameTX;
    }


    /**
     * Gets the cstConceptCodeLI value for this SiteABOType.
     * 
     * @return cstConceptCodeLI
     */
    public java.lang.String getCstConceptCodeLI() {
        return cstConceptCodeLI;
    }


    /**
     * Sets the cstConceptCodeLI value for this SiteABOType.
     * 
     * @param cstConceptCodeLI
     */
    public void setCstConceptCodeLI(java.lang.String cstConceptCodeLI) {
        this.cstConceptCodeLI = cstConceptCodeLI;
    }


    /**
     * Gets the cstLocationTypeLI value for this SiteABOType.
     * 
     * @return cstLocationTypeLI
     */
    public java.lang.String getCstLocationTypeLI() {
        return cstLocationTypeLI;
    }


    /**
     * Sets the cstLocationTypeLI value for this SiteABOType.
     * 
     * @param cstLocationTypeLI
     */
    public void setCstLocationTypeLI(java.lang.String cstLocationTypeLI) {
        this.cstLocationTypeLI = cstLocationTypeLI;
    }


    /**
     * Gets the cstLocationNumberTX value for this SiteABOType.
     * 
     * @return cstLocationNumberTX
     */
    public java.lang.String getCstLocationNumberTX() {
        return cstLocationNumberTX;
    }


    /**
     * Sets the cstLocationNumberTX value for this SiteABOType.
     * 
     * @param cstLocationNumberTX
     */
    public void setCstLocationNumberTX(java.lang.String cstLocationNumberTX) {
        this.cstLocationNumberTX = cstLocationNumberTX;
    }


    /**
     * Gets the triCityTX value for this SiteABOType.
     * 
     * @return triCityTX
     */
    public java.lang.String getTriCityTX() {
        return triCityTX;
    }


    /**
     * Sets the triCityTX value for this SiteABOType.
     * 
     * @param triCityTX
     */
    public void setTriCityTX(java.lang.String triCityTX) {
        this.triCityTX = triCityTX;
    }


    /**
     * Gets the triStateProvTX value for this SiteABOType.
     * 
     * @return triStateProvTX
     */
    public java.lang.String getTriStateProvTX() {
        return triStateProvTX;
    }


    /**
     * Sets the triStateProvTX value for this SiteABOType.
     * 
     * @param triStateProvTX
     */
    public void setTriStateProvTX(java.lang.String triStateProvTX) {
        this.triStateProvTX = triStateProvTX;
    }


    /**
     * Gets the triLatitudeTX value for this SiteABOType.
     * 
     * @return triLatitudeTX
     */
    public java.lang.String getTriLatitudeTX() {
        return triLatitudeTX;
    }


    /**
     * Sets the triLatitudeTX value for this SiteABOType.
     * 
     * @param triLatitudeTX
     */
    public void setTriLatitudeTX(java.lang.String triLatitudeTX) {
        this.triLatitudeTX = triLatitudeTX;
    }


    /**
     * Gets the triLongitudeTX value for this SiteABOType.
     * 
     * @return triLongitudeTX
     */
    public java.lang.String getTriLongitudeTX() {
        return triLongitudeTX;
    }


    /**
     * Sets the triLongitudeTX value for this SiteABOType.
     * 
     * @param triLongitudeTX
     */
    public void setTriLongitudeTX(java.lang.String triLongitudeTX) {
        this.triLongitudeTX = triLongitudeTX;
    }


    /**
     * Gets the cstLandSizeSqftNU value for this SiteABOType.
     * 
     * @return cstLandSizeSqftNU
     */
    public java.math.BigDecimal getCstLandSizeSqftNU() {
        return cstLandSizeSqftNU;
    }


    /**
     * Sets the cstLandSizeSqftNU value for this SiteABOType.
     * 
     * @param cstLandSizeSqftNU
     */
    public void setCstLandSizeSqftNU(java.math.BigDecimal cstLandSizeSqftNU) {
        this.cstLandSizeSqftNU = cstLandSizeSqftNU;
    }


    /**
     * Gets the cstLandSizeAcresNU value for this SiteABOType.
     * 
     * @return cstLandSizeAcresNU
     */
    public java.math.BigDecimal getCstLandSizeAcresNU() {
        return cstLandSizeAcresNU;
    }


    /**
     * Sets the cstLandSizeAcresNU value for this SiteABOType.
     * 
     * @param cstLandSizeAcresNU
     */
    public void setCstLandSizeAcresNU(java.math.BigDecimal cstLandSizeAcresNU) {
        this.cstLandSizeAcresNU = cstLandSizeAcresNU;
    }


    /**
     * Gets the cstVaporIntrusionLI value for this SiteABOType.
     * 
     * @return cstVaporIntrusionLI
     */
    public java.lang.String getCstVaporIntrusionLI() {
        return cstVaporIntrusionLI;
    }


    /**
     * Sets the cstVaporIntrusionLI value for this SiteABOType.
     * 
     * @param cstVaporIntrusionLI
     */
    public void setCstVaporIntrusionLI(java.lang.String cstVaporIntrusionLI) {
        this.cstVaporIntrusionLI = cstVaporIntrusionLI;
    }


    /**
     * Gets the cstMitigationSystemOtherTX value for this SiteABOType.
     * 
     * @return cstMitigationSystemOtherTX
     */
    public java.lang.String getCstMitigationSystemOtherTX() {
        return cstMitigationSystemOtherTX;
    }


    /**
     * Sets the cstMitigationSystemOtherTX value for this SiteABOType.
     * 
     * @param cstMitigationSystemOtherTX
     */
    public void setCstMitigationSystemOtherTX(java.lang.String cstMitigationSystemOtherTX) {
        this.cstMitigationSystemOtherTX = cstMitigationSystemOtherTX;
    }


    /**
     * Gets the cstMitigationSystemBL value for this SiteABOType.
     * 
     * @return cstMitigationSystemBL
     */
    public java.lang.String getCstMitigationSystemBL() {
        return cstMitigationSystemBL;
    }


    /**
     * Sets the cstMitigationSystemBL value for this SiteABOType.
     * 
     * @param cstMitigationSystemBL
     */
    public void setCstMitigationSystemBL(java.lang.String cstMitigationSystemBL) {
        this.cstMitigationSystemBL = cstMitigationSystemBL;
    }


    /**
     * Gets the closestExistingStore value for this SiteABOType.
     * 
     * @return closestExistingStore
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.StoreType[] getClosestExistingStore() {
        return closestExistingStore;
    }


    /**
     * Sets the closestExistingStore value for this SiteABOType.
     * 
     * @param closestExistingStore
     */
    public void setClosestExistingStore(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.StoreType[] closestExistingStore) {
        this.closestExistingStore = closestExistingStore;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.StoreType getClosestExistingStore(int i) {
        return this.closestExistingStore[i];
    }

    public void setClosestExistingStore(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.StoreType _value) {
        this.closestExistingStore[i] = _value;
    }


    /**
     * Gets the task value for this SiteABOType.
     * 
     * @return task
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.TaskType[] getTask() {
        return task;
    }


    /**
     * Sets the task value for this SiteABOType.
     * 
     * @param task
     */
    public void setTask(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.TaskType[] task) {
        this.task = task;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.TaskType getTask(int i) {
        return this.task[i];
    }

    public void setTask(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.TaskType _value) {
        this.task[i] = _value;
    }


    /**
     * Gets the checklist value for this SiteABOType.
     * 
     * @return checklist
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] getChecklist() {
        return checklist;
    }


    /**
     * Sets the checklist value for this SiteABOType.
     * 
     * @param checklist
     */
    public void setChecklist(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] checklist) {
        this.checklist = checklist;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType getChecklist(int i) {
        return this.checklist[i];
    }

    public void setChecklist(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType _value) {
        this.checklist[i] = _value;
    }


    /**
     * Gets the contract value for this SiteABOType.
     * 
     * @return contract
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ContractType[] getContract() {
        return contract;
    }


    /**
     * Sets the contract value for this SiteABOType.
     * 
     * @param contract
     */
    public void setContract(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ContractType[] contract) {
        this.contract = contract;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ContractType getContract(int i) {
        return this.contract[i];
    }

    public void setContract(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ContractType _value) {
        this.contract[i] = _value;
    }


    /**
     * Gets the contacts value for this SiteABOType.
     * 
     * @return contacts
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] getContacts() {
        return contacts;
    }


    /**
     * Sets the contacts value for this SiteABOType.
     * 
     * @param contacts
     */
    public void setContacts(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] contacts) {
        this.contacts = contacts;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType getContacts(int i) {
        return this.contacts[i];
    }

    public void setContacts(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType _value) {
        this.contacts[i] = _value;
    }


    /**
     * Gets the researchForecasts value for this SiteABOType.
     * 
     * @return researchForecasts
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ResearchForecastsType getResearchForecasts() {
        return researchForecasts;
    }


    /**
     * Sets the researchForecasts value for this SiteABOType.
     * 
     * @param researchForecasts
     */
    public void setResearchForecasts(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.ResearchForecastsType researchForecasts) {
        this.researchForecasts = researchForecasts;
    }


    /**
     * Gets the cstLegalNotesNO value for this SiteABOType.
     * 
     * @return cstLegalNotesNO
     */
    public java.lang.String getCstLegalNotesNO() {
        return cstLegalNotesNO;
    }


    /**
     * Sets the cstLegalNotesNO value for this SiteABOType.
     * 
     * @param cstLegalNotesNO
     */
    public void setCstLegalNotesNO(java.lang.String cstLegalNotesNO) {
        this.cstLegalNotesNO = cstLegalNotesNO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SiteABOType)) return false;
        SiteABOType other = (SiteABOType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cstSiteIdTX==null && other.getCstSiteIdTX()==null) || 
             (this.cstSiteIdTX!=null &&
              this.cstSiteIdTX.equals(other.getCstSiteIdTX()))) &&
            ((this.messageIdNU==null && other.getMessageIdNU()==null) || 
             (this.messageIdNU!=null &&
              this.messageIdNU.equals(other.getMessageIdNU()))) &&
            ((this.cstSiteStatusCL==null && other.getCstSiteStatusCL()==null) || 
             (this.cstSiteStatusCL!=null &&
              this.cstSiteStatusCL.equals(other.getCstSiteStatusCL()))) &&
            ((this.cstPreviousSiteStatusCL==null && other.getCstPreviousSiteStatusCL()==null) || 
             (this.cstPreviousSiteStatusCL!=null &&
              this.cstPreviousSiteStatusCL.equals(other.getCstPreviousSiteStatusCL()))) &&
            ((this.cstNameTX==null && other.getCstNameTX()==null) || 
             (this.cstNameTX!=null &&
              this.cstNameTX.equals(other.getCstNameTX()))) &&
            ((this.cstConceptCodeLI==null && other.getCstConceptCodeLI()==null) || 
             (this.cstConceptCodeLI!=null &&
              this.cstConceptCodeLI.equals(other.getCstConceptCodeLI()))) &&
            ((this.cstLocationTypeLI==null && other.getCstLocationTypeLI()==null) || 
             (this.cstLocationTypeLI!=null &&
              this.cstLocationTypeLI.equals(other.getCstLocationTypeLI()))) &&
            ((this.cstLocationNumberTX==null && other.getCstLocationNumberTX()==null) || 
             (this.cstLocationNumberTX!=null &&
              this.cstLocationNumberTX.equals(other.getCstLocationNumberTX()))) &&
            ((this.triCityTX==null && other.getTriCityTX()==null) || 
             (this.triCityTX!=null &&
              this.triCityTX.equals(other.getTriCityTX()))) &&
            ((this.triStateProvTX==null && other.getTriStateProvTX()==null) || 
             (this.triStateProvTX!=null &&
              this.triStateProvTX.equals(other.getTriStateProvTX()))) &&
            ((this.triLatitudeTX==null && other.getTriLatitudeTX()==null) || 
             (this.triLatitudeTX!=null &&
              this.triLatitudeTX.equals(other.getTriLatitudeTX()))) &&
            ((this.triLongitudeTX==null && other.getTriLongitudeTX()==null) || 
             (this.triLongitudeTX!=null &&
              this.triLongitudeTX.equals(other.getTriLongitudeTX()))) &&
            ((this.cstLandSizeSqftNU==null && other.getCstLandSizeSqftNU()==null) || 
             (this.cstLandSizeSqftNU!=null &&
              this.cstLandSizeSqftNU.equals(other.getCstLandSizeSqftNU()))) &&
            ((this.cstLandSizeAcresNU==null && other.getCstLandSizeAcresNU()==null) || 
             (this.cstLandSizeAcresNU!=null &&
              this.cstLandSizeAcresNU.equals(other.getCstLandSizeAcresNU()))) &&
            ((this.cstVaporIntrusionLI==null && other.getCstVaporIntrusionLI()==null) || 
             (this.cstVaporIntrusionLI!=null &&
              this.cstVaporIntrusionLI.equals(other.getCstVaporIntrusionLI()))) &&
            ((this.cstMitigationSystemOtherTX==null && other.getCstMitigationSystemOtherTX()==null) || 
             (this.cstMitigationSystemOtherTX!=null &&
              this.cstMitigationSystemOtherTX.equals(other.getCstMitigationSystemOtherTX()))) &&
            ((this.cstMitigationSystemBL==null && other.getCstMitigationSystemBL()==null) || 
             (this.cstMitigationSystemBL!=null &&
              this.cstMitigationSystemBL.equals(other.getCstMitigationSystemBL()))) &&
            ((this.closestExistingStore==null && other.getClosestExistingStore()==null) || 
             (this.closestExistingStore!=null &&
              java.util.Arrays.equals(this.closestExistingStore, other.getClosestExistingStore()))) &&
            ((this.task==null && other.getTask()==null) || 
             (this.task!=null &&
              java.util.Arrays.equals(this.task, other.getTask()))) &&
            ((this.checklist==null && other.getChecklist()==null) || 
             (this.checklist!=null &&
              java.util.Arrays.equals(this.checklist, other.getChecklist()))) &&
            ((this.contract==null && other.getContract()==null) || 
             (this.contract!=null &&
              java.util.Arrays.equals(this.contract, other.getContract()))) &&
            ((this.contacts==null && other.getContacts()==null) || 
             (this.contacts!=null &&
              java.util.Arrays.equals(this.contacts, other.getContacts()))) &&
            ((this.researchForecasts==null && other.getResearchForecasts()==null) || 
             (this.researchForecasts!=null &&
              this.researchForecasts.equals(other.getResearchForecasts()))) &&
            ((this.cstLegalNotesNO==null && other.getCstLegalNotesNO()==null) || 
             (this.cstLegalNotesNO!=null &&
              this.cstLegalNotesNO.equals(other.getCstLegalNotesNO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCstSiteIdTX() != null) {
            _hashCode += getCstSiteIdTX().hashCode();
        }
        if (getMessageIdNU() != null) {
            _hashCode += getMessageIdNU().hashCode();
        }
        if (getCstSiteStatusCL() != null) {
            _hashCode += getCstSiteStatusCL().hashCode();
        }
        if (getCstPreviousSiteStatusCL() != null) {
            _hashCode += getCstPreviousSiteStatusCL().hashCode();
        }
        if (getCstNameTX() != null) {
            _hashCode += getCstNameTX().hashCode();
        }
        if (getCstConceptCodeLI() != null) {
            _hashCode += getCstConceptCodeLI().hashCode();
        }
        if (getCstLocationTypeLI() != null) {
            _hashCode += getCstLocationTypeLI().hashCode();
        }
        if (getCstLocationNumberTX() != null) {
            _hashCode += getCstLocationNumberTX().hashCode();
        }
        if (getTriCityTX() != null) {
            _hashCode += getTriCityTX().hashCode();
        }
        if (getTriStateProvTX() != null) {
            _hashCode += getTriStateProvTX().hashCode();
        }
        if (getTriLatitudeTX() != null) {
            _hashCode += getTriLatitudeTX().hashCode();
        }
        if (getTriLongitudeTX() != null) {
            _hashCode += getTriLongitudeTX().hashCode();
        }
        if (getCstLandSizeSqftNU() != null) {
            _hashCode += getCstLandSizeSqftNU().hashCode();
        }
        if (getCstLandSizeAcresNU() != null) {
            _hashCode += getCstLandSizeAcresNU().hashCode();
        }
        if (getCstVaporIntrusionLI() != null) {
            _hashCode += getCstVaporIntrusionLI().hashCode();
        }
        if (getCstMitigationSystemOtherTX() != null) {
            _hashCode += getCstMitigationSystemOtherTX().hashCode();
        }
        if (getCstMitigationSystemBL() != null) {
            _hashCode += getCstMitigationSystemBL().hashCode();
        }
        if (getClosestExistingStore() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClosestExistingStore());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClosestExistingStore(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTask() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTask());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTask(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getChecklist() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChecklist());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChecklist(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getContract() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContract());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContract(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getContacts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContacts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContacts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResearchForecasts() != null) {
            _hashCode += getResearchForecasts().hashCode();
        }
        if (getCstLegalNotesNO() != null) {
            _hashCode += getCstLegalNotesNO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SiteABOType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "SiteABOType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstSiteIdTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstSiteIdTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageIdNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "messageIdNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstSiteStatusCL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstSiteStatusCL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstPreviousSiteStatusCL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstPreviousSiteStatusCL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstNameTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstNameTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstConceptCodeLI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstConceptCodeLI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstLocationTypeLI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstLocationTypeLI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstLocationNumberTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstLocationNumberTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triCityTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triCityTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triStateProvTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triStateProvTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triLatitudeTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triLatitudeTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triLongitudeTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triLongitudeTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstLandSizeSqftNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstLandSizeSqftNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstLandSizeAcresNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstLandSizeAcresNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstVaporIntrusionLI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstVaporIntrusionLI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstMitigationSystemOtherTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstMitigationSystemOtherTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstMitigationSystemBL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstMitigationSystemBL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closestExistingStore");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "closestExistingStore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "StoreType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("task");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "task"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "TaskType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checklist");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "checklist"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "CheckListType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contract");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "contract"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "ContractType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contacts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "contacts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "PersonType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("researchForecasts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "researchForecasts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "ResearchForecastsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstLegalNotesNO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstLegalNotesNO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
