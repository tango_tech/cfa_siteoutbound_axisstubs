/**
 * ContractType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class ContractType  implements java.io.Serializable {
    private java.lang.String triIdTX;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType initialBusinessTerm;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType finalBusinessTerm;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] criticalDate;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] contractContact;

    public ContractType() {
    }

    public ContractType(
           java.lang.String triIdTX,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType initialBusinessTerm,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType finalBusinessTerm,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] criticalDate,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] contractContact) {
           this.triIdTX = triIdTX;
           this.initialBusinessTerm = initialBusinessTerm;
           this.finalBusinessTerm = finalBusinessTerm;
           this.criticalDate = criticalDate;
           this.contractContact = contractContact;
    }


    /**
     * Gets the triIdTX value for this ContractType.
     * 
     * @return triIdTX
     */
    public java.lang.String getTriIdTX() {
        return triIdTX;
    }


    /**
     * Sets the triIdTX value for this ContractType.
     * 
     * @param triIdTX
     */
    public void setTriIdTX(java.lang.String triIdTX) {
        this.triIdTX = triIdTX;
    }


    /**
     * Gets the initialBusinessTerm value for this ContractType.
     * 
     * @return initialBusinessTerm
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType getInitialBusinessTerm() {
        return initialBusinessTerm;
    }


    /**
     * Sets the initialBusinessTerm value for this ContractType.
     * 
     * @param initialBusinessTerm
     */
    public void setInitialBusinessTerm(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType initialBusinessTerm) {
        this.initialBusinessTerm = initialBusinessTerm;
    }


    /**
     * Gets the finalBusinessTerm value for this ContractType.
     * 
     * @return finalBusinessTerm
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType getFinalBusinessTerm() {
        return finalBusinessTerm;
    }


    /**
     * Sets the finalBusinessTerm value for this ContractType.
     * 
     * @param finalBusinessTerm
     */
    public void setFinalBusinessTerm(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.BusinessTermsType finalBusinessTerm) {
        this.finalBusinessTerm = finalBusinessTerm;
    }


    /**
     * Gets the criticalDate value for this ContractType.
     * 
     * @return criticalDate
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] getCriticalDate() {
        return criticalDate;
    }


    /**
     * Sets the criticalDate value for this ContractType.
     * 
     * @param criticalDate
     */
    public void setCriticalDate(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType[] criticalDate) {
        this.criticalDate = criticalDate;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType getCriticalDate(int i) {
        return this.criticalDate[i];
    }

    public void setCriticalDate(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CheckListType _value) {
        this.criticalDate[i] = _value;
    }


    /**
     * Gets the contractContact value for this ContractType.
     * 
     * @return contractContact
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] getContractContact() {
        return contractContact;
    }


    /**
     * Sets the contractContact value for this ContractType.
     * 
     * @param contractContact
     */
    public void setContractContact(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] contractContact) {
        this.contractContact = contractContact;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType getContractContact(int i) {
        return this.contractContact[i];
    }

    public void setContractContact(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType _value) {
        this.contractContact[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ContractType)) return false;
        ContractType other = (ContractType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.triIdTX==null && other.getTriIdTX()==null) || 
             (this.triIdTX!=null &&
              this.triIdTX.equals(other.getTriIdTX()))) &&
            ((this.initialBusinessTerm==null && other.getInitialBusinessTerm()==null) || 
             (this.initialBusinessTerm!=null &&
              this.initialBusinessTerm.equals(other.getInitialBusinessTerm()))) &&
            ((this.finalBusinessTerm==null && other.getFinalBusinessTerm()==null) || 
             (this.finalBusinessTerm!=null &&
              this.finalBusinessTerm.equals(other.getFinalBusinessTerm()))) &&
            ((this.criticalDate==null && other.getCriticalDate()==null) || 
             (this.criticalDate!=null &&
              java.util.Arrays.equals(this.criticalDate, other.getCriticalDate()))) &&
            ((this.contractContact==null && other.getContractContact()==null) || 
             (this.contractContact!=null &&
              java.util.Arrays.equals(this.contractContact, other.getContractContact())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTriIdTX() != null) {
            _hashCode += getTriIdTX().hashCode();
        }
        if (getInitialBusinessTerm() != null) {
            _hashCode += getInitialBusinessTerm().hashCode();
        }
        if (getFinalBusinessTerm() != null) {
            _hashCode += getFinalBusinessTerm().hashCode();
        }
        if (getCriticalDate() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCriticalDate());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCriticalDate(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getContractContact() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContractContact());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContractContact(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ContractType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "ContractType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIdTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triIdTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("initialBusinessTerm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "initialBusinessTerm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "BusinessTermsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finalBusinessTerm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "finalBusinessTerm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "BusinessTermsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criticalDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "criticalDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "CheckListType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contractContact");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "contractContact"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "PersonType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
