/**
 * PersonType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class PersonType  implements java.io.Serializable {
    private java.lang.String personIdTX;

    private java.lang.String personRoleLI;

    private java.lang.String internalOrExternal;

    private java.lang.String personOracleId;

    private java.lang.String personGUID;

    private java.lang.String triFirstNameTX;

    private java.lang.String triLastNameTX;

    private java.lang.String companyNameTX;

    private java.lang.String companyOracleIDTX;

    private java.lang.String companyTriIdTX;

    private java.lang.String triAddressTX;

    private java.lang.String triAddress02TX;

    private java.lang.String triAddress03TX;

    private java.lang.String triCityTX;

    private java.lang.String triStateProvTX;

    private java.lang.String triZipPostalTX;

    private java.lang.String triWorkPhoneTX;

    private java.lang.String triWorkFaxTX;

    private java.lang.String triMobileTX;

    private java.lang.String triEmailTX;

    private java.lang.String cstVendorSiteOracleIDTX;

    private java.lang.String vendorSiteTriIdTX;

    private java.lang.String vendorPMCompanySiteIDTX;

    public PersonType() {
    }

    public PersonType(
           java.lang.String personIdTX,
           java.lang.String personRoleLI,
           java.lang.String internalOrExternal,
           java.lang.String personOracleId,
           java.lang.String personGUID,
           java.lang.String triFirstNameTX,
           java.lang.String triLastNameTX,
           java.lang.String companyNameTX,
           java.lang.String companyOracleIDTX,
           java.lang.String companyTriIdTX,
           java.lang.String triAddressTX,
           java.lang.String triAddress02TX,
           java.lang.String triAddress03TX,
           java.lang.String triCityTX,
           java.lang.String triStateProvTX,
           java.lang.String triZipPostalTX,
           java.lang.String triWorkPhoneTX,
           java.lang.String triWorkFaxTX,
           java.lang.String triMobileTX,
           java.lang.String triEmailTX,
           java.lang.String cstVendorSiteOracleIDTX,
           java.lang.String vendorSiteTriIdTX,
           java.lang.String vendorPMCompanySiteIDTX) {
           this.personIdTX = personIdTX;
           this.personRoleLI = personRoleLI;
           this.internalOrExternal = internalOrExternal;
           this.personOracleId = personOracleId;
           this.personGUID = personGUID;
           this.triFirstNameTX = triFirstNameTX;
           this.triLastNameTX = triLastNameTX;
           this.companyNameTX = companyNameTX;
           this.companyOracleIDTX = companyOracleIDTX;
           this.companyTriIdTX = companyTriIdTX;
           this.triAddressTX = triAddressTX;
           this.triAddress02TX = triAddress02TX;
           this.triAddress03TX = triAddress03TX;
           this.triCityTX = triCityTX;
           this.triStateProvTX = triStateProvTX;
           this.triZipPostalTX = triZipPostalTX;
           this.triWorkPhoneTX = triWorkPhoneTX;
           this.triWorkFaxTX = triWorkFaxTX;
           this.triMobileTX = triMobileTX;
           this.triEmailTX = triEmailTX;
           this.cstVendorSiteOracleIDTX = cstVendorSiteOracleIDTX;
           this.vendorSiteTriIdTX = vendorSiteTriIdTX;
           this.vendorPMCompanySiteIDTX = vendorPMCompanySiteIDTX;
    }


    /**
     * Gets the personIdTX value for this PersonType.
     * 
     * @return personIdTX
     */
    public java.lang.String getPersonIdTX() {
        return personIdTX;
    }


    /**
     * Sets the personIdTX value for this PersonType.
     * 
     * @param personIdTX
     */
    public void setPersonIdTX(java.lang.String personIdTX) {
        this.personIdTX = personIdTX;
    }


    /**
     * Gets the personRoleLI value for this PersonType.
     * 
     * @return personRoleLI
     */
    public java.lang.String getPersonRoleLI() {
        return personRoleLI;
    }


    /**
     * Sets the personRoleLI value for this PersonType.
     * 
     * @param personRoleLI
     */
    public void setPersonRoleLI(java.lang.String personRoleLI) {
        this.personRoleLI = personRoleLI;
    }


    /**
     * Gets the internalOrExternal value for this PersonType.
     * 
     * @return internalOrExternal
     */
    public java.lang.String getInternalOrExternal() {
        return internalOrExternal;
    }


    /**
     * Sets the internalOrExternal value for this PersonType.
     * 
     * @param internalOrExternal
     */
    public void setInternalOrExternal(java.lang.String internalOrExternal) {
        this.internalOrExternal = internalOrExternal;
    }


    /**
     * Gets the personOracleId value for this PersonType.
     * 
     * @return personOracleId
     */
    public java.lang.String getPersonOracleId() {
        return personOracleId;
    }


    /**
     * Sets the personOracleId value for this PersonType.
     * 
     * @param personOracleId
     */
    public void setPersonOracleId(java.lang.String personOracleId) {
        this.personOracleId = personOracleId;
    }


    /**
     * Gets the personGUID value for this PersonType.
     * 
     * @return personGUID
     */
    public java.lang.String getPersonGUID() {
        return personGUID;
    }


    /**
     * Sets the personGUID value for this PersonType.
     * 
     * @param personGUID
     */
    public void setPersonGUID(java.lang.String personGUID) {
        this.personGUID = personGUID;
    }


    /**
     * Gets the triFirstNameTX value for this PersonType.
     * 
     * @return triFirstNameTX
     */
    public java.lang.String getTriFirstNameTX() {
        return triFirstNameTX;
    }


    /**
     * Sets the triFirstNameTX value for this PersonType.
     * 
     * @param triFirstNameTX
     */
    public void setTriFirstNameTX(java.lang.String triFirstNameTX) {
        this.triFirstNameTX = triFirstNameTX;
    }


    /**
     * Gets the triLastNameTX value for this PersonType.
     * 
     * @return triLastNameTX
     */
    public java.lang.String getTriLastNameTX() {
        return triLastNameTX;
    }


    /**
     * Sets the triLastNameTX value for this PersonType.
     * 
     * @param triLastNameTX
     */
    public void setTriLastNameTX(java.lang.String triLastNameTX) {
        this.triLastNameTX = triLastNameTX;
    }


    /**
     * Gets the companyNameTX value for this PersonType.
     * 
     * @return companyNameTX
     */
    public java.lang.String getCompanyNameTX() {
        return companyNameTX;
    }


    /**
     * Sets the companyNameTX value for this PersonType.
     * 
     * @param companyNameTX
     */
    public void setCompanyNameTX(java.lang.String companyNameTX) {
        this.companyNameTX = companyNameTX;
    }


    /**
     * Gets the companyOracleIDTX value for this PersonType.
     * 
     * @return companyOracleIDTX
     */
    public java.lang.String getCompanyOracleIDTX() {
        return companyOracleIDTX;
    }


    /**
     * Sets the companyOracleIDTX value for this PersonType.
     * 
     * @param companyOracleIDTX
     */
    public void setCompanyOracleIDTX(java.lang.String companyOracleIDTX) {
        this.companyOracleIDTX = companyOracleIDTX;
    }


    /**
     * Gets the companyTriIdTX value for this PersonType.
     * 
     * @return companyTriIdTX
     */
    public java.lang.String getCompanyTriIdTX() {
        return companyTriIdTX;
    }


    /**
     * Sets the companyTriIdTX value for this PersonType.
     * 
     * @param companyTriIdTX
     */
    public void setCompanyTriIdTX(java.lang.String companyTriIdTX) {
        this.companyTriIdTX = companyTriIdTX;
    }


    /**
     * Gets the triAddressTX value for this PersonType.
     * 
     * @return triAddressTX
     */
    public java.lang.String getTriAddressTX() {
        return triAddressTX;
    }


    /**
     * Sets the triAddressTX value for this PersonType.
     * 
     * @param triAddressTX
     */
    public void setTriAddressTX(java.lang.String triAddressTX) {
        this.triAddressTX = triAddressTX;
    }


    /**
     * Gets the triAddress02TX value for this PersonType.
     * 
     * @return triAddress02TX
     */
    public java.lang.String getTriAddress02TX() {
        return triAddress02TX;
    }


    /**
     * Sets the triAddress02TX value for this PersonType.
     * 
     * @param triAddress02TX
     */
    public void setTriAddress02TX(java.lang.String triAddress02TX) {
        this.triAddress02TX = triAddress02TX;
    }


    /**
     * Gets the triAddress03TX value for this PersonType.
     * 
     * @return triAddress03TX
     */
    public java.lang.String getTriAddress03TX() {
        return triAddress03TX;
    }


    /**
     * Sets the triAddress03TX value for this PersonType.
     * 
     * @param triAddress03TX
     */
    public void setTriAddress03TX(java.lang.String triAddress03TX) {
        this.triAddress03TX = triAddress03TX;
    }


    /**
     * Gets the triCityTX value for this PersonType.
     * 
     * @return triCityTX
     */
    public java.lang.String getTriCityTX() {
        return triCityTX;
    }


    /**
     * Sets the triCityTX value for this PersonType.
     * 
     * @param triCityTX
     */
    public void setTriCityTX(java.lang.String triCityTX) {
        this.triCityTX = triCityTX;
    }


    /**
     * Gets the triStateProvTX value for this PersonType.
     * 
     * @return triStateProvTX
     */
    public java.lang.String getTriStateProvTX() {
        return triStateProvTX;
    }


    /**
     * Sets the triStateProvTX value for this PersonType.
     * 
     * @param triStateProvTX
     */
    public void setTriStateProvTX(java.lang.String triStateProvTX) {
        this.triStateProvTX = triStateProvTX;
    }


    /**
     * Gets the triZipPostalTX value for this PersonType.
     * 
     * @return triZipPostalTX
     */
    public java.lang.String getTriZipPostalTX() {
        return triZipPostalTX;
    }


    /**
     * Sets the triZipPostalTX value for this PersonType.
     * 
     * @param triZipPostalTX
     */
    public void setTriZipPostalTX(java.lang.String triZipPostalTX) {
        this.triZipPostalTX = triZipPostalTX;
    }


    /**
     * Gets the triWorkPhoneTX value for this PersonType.
     * 
     * @return triWorkPhoneTX
     */
    public java.lang.String getTriWorkPhoneTX() {
        return triWorkPhoneTX;
    }


    /**
     * Sets the triWorkPhoneTX value for this PersonType.
     * 
     * @param triWorkPhoneTX
     */
    public void setTriWorkPhoneTX(java.lang.String triWorkPhoneTX) {
        this.triWorkPhoneTX = triWorkPhoneTX;
    }


    /**
     * Gets the triWorkFaxTX value for this PersonType.
     * 
     * @return triWorkFaxTX
     */
    public java.lang.String getTriWorkFaxTX() {
        return triWorkFaxTX;
    }


    /**
     * Sets the triWorkFaxTX value for this PersonType.
     * 
     * @param triWorkFaxTX
     */
    public void setTriWorkFaxTX(java.lang.String triWorkFaxTX) {
        this.triWorkFaxTX = triWorkFaxTX;
    }


    /**
     * Gets the triMobileTX value for this PersonType.
     * 
     * @return triMobileTX
     */
    public java.lang.String getTriMobileTX() {
        return triMobileTX;
    }


    /**
     * Sets the triMobileTX value for this PersonType.
     * 
     * @param triMobileTX
     */
    public void setTriMobileTX(java.lang.String triMobileTX) {
        this.triMobileTX = triMobileTX;
    }


    /**
     * Gets the triEmailTX value for this PersonType.
     * 
     * @return triEmailTX
     */
    public java.lang.String getTriEmailTX() {
        return triEmailTX;
    }


    /**
     * Sets the triEmailTX value for this PersonType.
     * 
     * @param triEmailTX
     */
    public void setTriEmailTX(java.lang.String triEmailTX) {
        this.triEmailTX = triEmailTX;
    }


    /**
     * Gets the cstVendorSiteOracleIDTX value for this PersonType.
     * 
     * @return cstVendorSiteOracleIDTX
     */
    public java.lang.String getCstVendorSiteOracleIDTX() {
        return cstVendorSiteOracleIDTX;
    }


    /**
     * Sets the cstVendorSiteOracleIDTX value for this PersonType.
     * 
     * @param cstVendorSiteOracleIDTX
     */
    public void setCstVendorSiteOracleIDTX(java.lang.String cstVendorSiteOracleIDTX) {
        this.cstVendorSiteOracleIDTX = cstVendorSiteOracleIDTX;
    }


    /**
     * Gets the vendorSiteTriIdTX value for this PersonType.
     * 
     * @return vendorSiteTriIdTX
     */
    public java.lang.String getVendorSiteTriIdTX() {
        return vendorSiteTriIdTX;
    }


    /**
     * Sets the vendorSiteTriIdTX value for this PersonType.
     * 
     * @param vendorSiteTriIdTX
     */
    public void setVendorSiteTriIdTX(java.lang.String vendorSiteTriIdTX) {
        this.vendorSiteTriIdTX = vendorSiteTriIdTX;
    }


    /**
     * Gets the vendorPMCompanySiteIDTX value for this PersonType.
     * 
     * @return vendorPMCompanySiteIDTX
     */
    public java.lang.String getVendorPMCompanySiteIDTX() {
        return vendorPMCompanySiteIDTX;
    }


    /**
     * Sets the vendorPMCompanySiteIDTX value for this PersonType.
     * 
     * @param vendorPMCompanySiteIDTX
     */
    public void setVendorPMCompanySiteIDTX(java.lang.String vendorPMCompanySiteIDTX) {
        this.vendorPMCompanySiteIDTX = vendorPMCompanySiteIDTX;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PersonType)) return false;
        PersonType other = (PersonType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.personIdTX==null && other.getPersonIdTX()==null) || 
             (this.personIdTX!=null &&
              this.personIdTX.equals(other.getPersonIdTX()))) &&
            ((this.personRoleLI==null && other.getPersonRoleLI()==null) || 
             (this.personRoleLI!=null &&
              this.personRoleLI.equals(other.getPersonRoleLI()))) &&
            ((this.internalOrExternal==null && other.getInternalOrExternal()==null) || 
             (this.internalOrExternal!=null &&
              this.internalOrExternal.equals(other.getInternalOrExternal()))) &&
            ((this.personOracleId==null && other.getPersonOracleId()==null) || 
             (this.personOracleId!=null &&
              this.personOracleId.equals(other.getPersonOracleId()))) &&
            ((this.personGUID==null && other.getPersonGUID()==null) || 
             (this.personGUID!=null &&
              this.personGUID.equals(other.getPersonGUID()))) &&
            ((this.triFirstNameTX==null && other.getTriFirstNameTX()==null) || 
             (this.triFirstNameTX!=null &&
              this.triFirstNameTX.equals(other.getTriFirstNameTX()))) &&
            ((this.triLastNameTX==null && other.getTriLastNameTX()==null) || 
             (this.triLastNameTX!=null &&
              this.triLastNameTX.equals(other.getTriLastNameTX()))) &&
            ((this.companyNameTX==null && other.getCompanyNameTX()==null) || 
             (this.companyNameTX!=null &&
              this.companyNameTX.equals(other.getCompanyNameTX()))) &&
            ((this.companyOracleIDTX==null && other.getCompanyOracleIDTX()==null) || 
             (this.companyOracleIDTX!=null &&
              this.companyOracleIDTX.equals(other.getCompanyOracleIDTX()))) &&
            ((this.companyTriIdTX==null && other.getCompanyTriIdTX()==null) || 
             (this.companyTriIdTX!=null &&
              this.companyTriIdTX.equals(other.getCompanyTriIdTX()))) &&
            ((this.triAddressTX==null && other.getTriAddressTX()==null) || 
             (this.triAddressTX!=null &&
              this.triAddressTX.equals(other.getTriAddressTX()))) &&
            ((this.triAddress02TX==null && other.getTriAddress02TX()==null) || 
             (this.triAddress02TX!=null &&
              this.triAddress02TX.equals(other.getTriAddress02TX()))) &&
            ((this.triAddress03TX==null && other.getTriAddress03TX()==null) || 
             (this.triAddress03TX!=null &&
              this.triAddress03TX.equals(other.getTriAddress03TX()))) &&
            ((this.triCityTX==null && other.getTriCityTX()==null) || 
             (this.triCityTX!=null &&
              this.triCityTX.equals(other.getTriCityTX()))) &&
            ((this.triStateProvTX==null && other.getTriStateProvTX()==null) || 
             (this.triStateProvTX!=null &&
              this.triStateProvTX.equals(other.getTriStateProvTX()))) &&
            ((this.triZipPostalTX==null && other.getTriZipPostalTX()==null) || 
             (this.triZipPostalTX!=null &&
              this.triZipPostalTX.equals(other.getTriZipPostalTX()))) &&
            ((this.triWorkPhoneTX==null && other.getTriWorkPhoneTX()==null) || 
             (this.triWorkPhoneTX!=null &&
              this.triWorkPhoneTX.equals(other.getTriWorkPhoneTX()))) &&
            ((this.triWorkFaxTX==null && other.getTriWorkFaxTX()==null) || 
             (this.triWorkFaxTX!=null &&
              this.triWorkFaxTX.equals(other.getTriWorkFaxTX()))) &&
            ((this.triMobileTX==null && other.getTriMobileTX()==null) || 
             (this.triMobileTX!=null &&
              this.triMobileTX.equals(other.getTriMobileTX()))) &&
            ((this.triEmailTX==null && other.getTriEmailTX()==null) || 
             (this.triEmailTX!=null &&
              this.triEmailTX.equals(other.getTriEmailTX()))) &&
            ((this.cstVendorSiteOracleIDTX==null && other.getCstVendorSiteOracleIDTX()==null) || 
             (this.cstVendorSiteOracleIDTX!=null &&
              this.cstVendorSiteOracleIDTX.equals(other.getCstVendorSiteOracleIDTX()))) &&
            ((this.vendorSiteTriIdTX==null && other.getVendorSiteTriIdTX()==null) || 
             (this.vendorSiteTriIdTX!=null &&
              this.vendorSiteTriIdTX.equals(other.getVendorSiteTriIdTX()))) &&
            ((this.vendorPMCompanySiteIDTX==null && other.getVendorPMCompanySiteIDTX()==null) || 
             (this.vendorPMCompanySiteIDTX!=null &&
              this.vendorPMCompanySiteIDTX.equals(other.getVendorPMCompanySiteIDTX())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPersonIdTX() != null) {
            _hashCode += getPersonIdTX().hashCode();
        }
        if (getPersonRoleLI() != null) {
            _hashCode += getPersonRoleLI().hashCode();
        }
        if (getInternalOrExternal() != null) {
            _hashCode += getInternalOrExternal().hashCode();
        }
        if (getPersonOracleId() != null) {
            _hashCode += getPersonOracleId().hashCode();
        }
        if (getPersonGUID() != null) {
            _hashCode += getPersonGUID().hashCode();
        }
        if (getTriFirstNameTX() != null) {
            _hashCode += getTriFirstNameTX().hashCode();
        }
        if (getTriLastNameTX() != null) {
            _hashCode += getTriLastNameTX().hashCode();
        }
        if (getCompanyNameTX() != null) {
            _hashCode += getCompanyNameTX().hashCode();
        }
        if (getCompanyOracleIDTX() != null) {
            _hashCode += getCompanyOracleIDTX().hashCode();
        }
        if (getCompanyTriIdTX() != null) {
            _hashCode += getCompanyTriIdTX().hashCode();
        }
        if (getTriAddressTX() != null) {
            _hashCode += getTriAddressTX().hashCode();
        }
        if (getTriAddress02TX() != null) {
            _hashCode += getTriAddress02TX().hashCode();
        }
        if (getTriAddress03TX() != null) {
            _hashCode += getTriAddress03TX().hashCode();
        }
        if (getTriCityTX() != null) {
            _hashCode += getTriCityTX().hashCode();
        }
        if (getTriStateProvTX() != null) {
            _hashCode += getTriStateProvTX().hashCode();
        }
        if (getTriZipPostalTX() != null) {
            _hashCode += getTriZipPostalTX().hashCode();
        }
        if (getTriWorkPhoneTX() != null) {
            _hashCode += getTriWorkPhoneTX().hashCode();
        }
        if (getTriWorkFaxTX() != null) {
            _hashCode += getTriWorkFaxTX().hashCode();
        }
        if (getTriMobileTX() != null) {
            _hashCode += getTriMobileTX().hashCode();
        }
        if (getTriEmailTX() != null) {
            _hashCode += getTriEmailTX().hashCode();
        }
        if (getCstVendorSiteOracleIDTX() != null) {
            _hashCode += getCstVendorSiteOracleIDTX().hashCode();
        }
        if (getVendorSiteTriIdTX() != null) {
            _hashCode += getVendorSiteTriIdTX().hashCode();
        }
        if (getVendorPMCompanySiteIDTX() != null) {
            _hashCode += getVendorPMCompanySiteIDTX().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PersonType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "PersonType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personIdTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "personIdTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personRoleLI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "personRoleLI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("internalOrExternal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "internalOrExternal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personOracleId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "personOracleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personGUID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "personGUID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triFirstNameTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triFirstNameTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triLastNameTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triLastNameTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyNameTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "companyNameTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyOracleIDTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "companyOracleIDTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyTriIdTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "companyTriIdTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triAddressTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triAddressTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triAddress02TX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triAddress02TX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triAddress03TX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triAddress03TX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triCityTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triCityTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triStateProvTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triStateProvTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triZipPostalTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triZipPostalTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triWorkPhoneTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triWorkPhoneTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triWorkFaxTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triWorkFaxTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triMobileTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triMobileTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triEmailTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triEmailTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstVendorSiteOracleIDTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstVendorSiteOracleIDTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorSiteTriIdTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "vendorSiteTriIdTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorPMCompanySiteIDTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "vendorPMCompanySiteIDTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
