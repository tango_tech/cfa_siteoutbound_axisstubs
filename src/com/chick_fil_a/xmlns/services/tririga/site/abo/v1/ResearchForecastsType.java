/**
 * ResearchForecastsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class ResearchForecastsType  implements java.io.Serializable {
    private java.util.Date cstResearchCompletionDateDT;

    private java.math.BigDecimal cstFirstYearSalesRangeMinNU;

    private java.math.BigDecimal cstFirstYearSalesRangeMaxNU;

    private java.math.BigDecimal cstThirdYearSalesRangeMinNU;

    private java.math.BigDecimal cstThirdYearSalesRangeMaxNU;

    private java.math.BigDecimal cstNetNewSalesLowNU;

    private java.math.BigDecimal cstNetNewSalesHighNU;

    public ResearchForecastsType() {
    }

    public ResearchForecastsType(
           java.util.Date cstResearchCompletionDateDT,
           java.math.BigDecimal cstFirstYearSalesRangeMinNU,
           java.math.BigDecimal cstFirstYearSalesRangeMaxNU,
           java.math.BigDecimal cstThirdYearSalesRangeMinNU,
           java.math.BigDecimal cstThirdYearSalesRangeMaxNU,
           java.math.BigDecimal cstNetNewSalesLowNU,
           java.math.BigDecimal cstNetNewSalesHighNU) {
           this.cstResearchCompletionDateDT = cstResearchCompletionDateDT;
           this.cstFirstYearSalesRangeMinNU = cstFirstYearSalesRangeMinNU;
           this.cstFirstYearSalesRangeMaxNU = cstFirstYearSalesRangeMaxNU;
           this.cstThirdYearSalesRangeMinNU = cstThirdYearSalesRangeMinNU;
           this.cstThirdYearSalesRangeMaxNU = cstThirdYearSalesRangeMaxNU;
           this.cstNetNewSalesLowNU = cstNetNewSalesLowNU;
           this.cstNetNewSalesHighNU = cstNetNewSalesHighNU;
    }


    /**
     * Gets the cstResearchCompletionDateDT value for this ResearchForecastsType.
     * 
     * @return cstResearchCompletionDateDT
     */
    public java.util.Date getCstResearchCompletionDateDT() {
        return cstResearchCompletionDateDT;
    }


    /**
     * Sets the cstResearchCompletionDateDT value for this ResearchForecastsType.
     * 
     * @param cstResearchCompletionDateDT
     */
    public void setCstResearchCompletionDateDT(java.util.Date cstResearchCompletionDateDT) {
        this.cstResearchCompletionDateDT = cstResearchCompletionDateDT;
    }


    /**
     * Gets the cstFirstYearSalesRangeMinNU value for this ResearchForecastsType.
     * 
     * @return cstFirstYearSalesRangeMinNU
     */
    public java.math.BigDecimal getCstFirstYearSalesRangeMinNU() {
        return cstFirstYearSalesRangeMinNU;
    }


    /**
     * Sets the cstFirstYearSalesRangeMinNU value for this ResearchForecastsType.
     * 
     * @param cstFirstYearSalesRangeMinNU
     */
    public void setCstFirstYearSalesRangeMinNU(java.math.BigDecimal cstFirstYearSalesRangeMinNU) {
        this.cstFirstYearSalesRangeMinNU = cstFirstYearSalesRangeMinNU;
    }


    /**
     * Gets the cstFirstYearSalesRangeMaxNU value for this ResearchForecastsType.
     * 
     * @return cstFirstYearSalesRangeMaxNU
     */
    public java.math.BigDecimal getCstFirstYearSalesRangeMaxNU() {
        return cstFirstYearSalesRangeMaxNU;
    }


    /**
     * Sets the cstFirstYearSalesRangeMaxNU value for this ResearchForecastsType.
     * 
     * @param cstFirstYearSalesRangeMaxNU
     */
    public void setCstFirstYearSalesRangeMaxNU(java.math.BigDecimal cstFirstYearSalesRangeMaxNU) {
        this.cstFirstYearSalesRangeMaxNU = cstFirstYearSalesRangeMaxNU;
    }


    /**
     * Gets the cstThirdYearSalesRangeMinNU value for this ResearchForecastsType.
     * 
     * @return cstThirdYearSalesRangeMinNU
     */
    public java.math.BigDecimal getCstThirdYearSalesRangeMinNU() {
        return cstThirdYearSalesRangeMinNU;
    }


    /**
     * Sets the cstThirdYearSalesRangeMinNU value for this ResearchForecastsType.
     * 
     * @param cstThirdYearSalesRangeMinNU
     */
    public void setCstThirdYearSalesRangeMinNU(java.math.BigDecimal cstThirdYearSalesRangeMinNU) {
        this.cstThirdYearSalesRangeMinNU = cstThirdYearSalesRangeMinNU;
    }


    /**
     * Gets the cstThirdYearSalesRangeMaxNU value for this ResearchForecastsType.
     * 
     * @return cstThirdYearSalesRangeMaxNU
     */
    public java.math.BigDecimal getCstThirdYearSalesRangeMaxNU() {
        return cstThirdYearSalesRangeMaxNU;
    }


    /**
     * Sets the cstThirdYearSalesRangeMaxNU value for this ResearchForecastsType.
     * 
     * @param cstThirdYearSalesRangeMaxNU
     */
    public void setCstThirdYearSalesRangeMaxNU(java.math.BigDecimal cstThirdYearSalesRangeMaxNU) {
        this.cstThirdYearSalesRangeMaxNU = cstThirdYearSalesRangeMaxNU;
    }


    /**
     * Gets the cstNetNewSalesLowNU value for this ResearchForecastsType.
     * 
     * @return cstNetNewSalesLowNU
     */
    public java.math.BigDecimal getCstNetNewSalesLowNU() {
        return cstNetNewSalesLowNU;
    }


    /**
     * Sets the cstNetNewSalesLowNU value for this ResearchForecastsType.
     * 
     * @param cstNetNewSalesLowNU
     */
    public void setCstNetNewSalesLowNU(java.math.BigDecimal cstNetNewSalesLowNU) {
        this.cstNetNewSalesLowNU = cstNetNewSalesLowNU;
    }


    /**
     * Gets the cstNetNewSalesHighNU value for this ResearchForecastsType.
     * 
     * @return cstNetNewSalesHighNU
     */
    public java.math.BigDecimal getCstNetNewSalesHighNU() {
        return cstNetNewSalesHighNU;
    }


    /**
     * Sets the cstNetNewSalesHighNU value for this ResearchForecastsType.
     * 
     * @param cstNetNewSalesHighNU
     */
    public void setCstNetNewSalesHighNU(java.math.BigDecimal cstNetNewSalesHighNU) {
        this.cstNetNewSalesHighNU = cstNetNewSalesHighNU;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResearchForecastsType)) return false;
        ResearchForecastsType other = (ResearchForecastsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cstResearchCompletionDateDT==null && other.getCstResearchCompletionDateDT()==null) || 
             (this.cstResearchCompletionDateDT!=null &&
              this.cstResearchCompletionDateDT.equals(other.getCstResearchCompletionDateDT()))) &&
            ((this.cstFirstYearSalesRangeMinNU==null && other.getCstFirstYearSalesRangeMinNU()==null) || 
             (this.cstFirstYearSalesRangeMinNU!=null &&
              this.cstFirstYearSalesRangeMinNU.equals(other.getCstFirstYearSalesRangeMinNU()))) &&
            ((this.cstFirstYearSalesRangeMaxNU==null && other.getCstFirstYearSalesRangeMaxNU()==null) || 
             (this.cstFirstYearSalesRangeMaxNU!=null &&
              this.cstFirstYearSalesRangeMaxNU.equals(other.getCstFirstYearSalesRangeMaxNU()))) &&
            ((this.cstThirdYearSalesRangeMinNU==null && other.getCstThirdYearSalesRangeMinNU()==null) || 
             (this.cstThirdYearSalesRangeMinNU!=null &&
              this.cstThirdYearSalesRangeMinNU.equals(other.getCstThirdYearSalesRangeMinNU()))) &&
            ((this.cstThirdYearSalesRangeMaxNU==null && other.getCstThirdYearSalesRangeMaxNU()==null) || 
             (this.cstThirdYearSalesRangeMaxNU!=null &&
              this.cstThirdYearSalesRangeMaxNU.equals(other.getCstThirdYearSalesRangeMaxNU()))) &&
            ((this.cstNetNewSalesLowNU==null && other.getCstNetNewSalesLowNU()==null) || 
             (this.cstNetNewSalesLowNU!=null &&
              this.cstNetNewSalesLowNU.equals(other.getCstNetNewSalesLowNU()))) &&
            ((this.cstNetNewSalesHighNU==null && other.getCstNetNewSalesHighNU()==null) || 
             (this.cstNetNewSalesHighNU!=null &&
              this.cstNetNewSalesHighNU.equals(other.getCstNetNewSalesHighNU())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCstResearchCompletionDateDT() != null) {
            _hashCode += getCstResearchCompletionDateDT().hashCode();
        }
        if (getCstFirstYearSalesRangeMinNU() != null) {
            _hashCode += getCstFirstYearSalesRangeMinNU().hashCode();
        }
        if (getCstFirstYearSalesRangeMaxNU() != null) {
            _hashCode += getCstFirstYearSalesRangeMaxNU().hashCode();
        }
        if (getCstThirdYearSalesRangeMinNU() != null) {
            _hashCode += getCstThirdYearSalesRangeMinNU().hashCode();
        }
        if (getCstThirdYearSalesRangeMaxNU() != null) {
            _hashCode += getCstThirdYearSalesRangeMaxNU().hashCode();
        }
        if (getCstNetNewSalesLowNU() != null) {
            _hashCode += getCstNetNewSalesLowNU().hashCode();
        }
        if (getCstNetNewSalesHighNU() != null) {
            _hashCode += getCstNetNewSalesHighNU().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResearchForecastsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "ResearchForecastsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstResearchCompletionDateDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstResearchCompletionDateDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstFirstYearSalesRangeMinNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstFirstYearSalesRangeMinNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstFirstYearSalesRangeMaxNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstFirstYearSalesRangeMaxNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstThirdYearSalesRangeMinNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstThirdYearSalesRangeMinNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstThirdYearSalesRangeMaxNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstThirdYearSalesRangeMaxNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstNetNewSalesLowNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstNetNewSalesLowNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstNetNewSalesHighNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstNetNewSalesHighNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
