/**
 * TaskType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class TaskType  implements java.io.Serializable {
    private java.lang.String triIdTX;

    private java.lang.String triNameTX;

    private java.lang.String triPlannedStartDT;

    private java.lang.String triPlannedEndDT;

    private java.lang.String triActualStartDT;

    private java.lang.String triActualEndDT;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] comment;

    public TaskType() {
    }

    public TaskType(
           java.lang.String triIdTX,
           java.lang.String triNameTX,
           java.lang.String triPlannedStartDT,
           java.lang.String triPlannedEndDT,
           java.lang.String triActualStartDT,
           java.lang.String triActualEndDT,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] comment) {
           this.triIdTX = triIdTX;
           this.triNameTX = triNameTX;
           this.triPlannedStartDT = triPlannedStartDT;
           this.triPlannedEndDT = triPlannedEndDT;
           this.triActualStartDT = triActualStartDT;
           this.triActualEndDT = triActualEndDT;
           this.comment = comment;
    }


    /**
     * Gets the triIdTX value for this TaskType.
     * 
     * @return triIdTX
     */
    public java.lang.String getTriIdTX() {
        return triIdTX;
    }


    /**
     * Sets the triIdTX value for this TaskType.
     * 
     * @param triIdTX
     */
    public void setTriIdTX(java.lang.String triIdTX) {
        this.triIdTX = triIdTX;
    }


    /**
     * Gets the triNameTX value for this TaskType.
     * 
     * @return triNameTX
     */
    public java.lang.String getTriNameTX() {
        return triNameTX;
    }


    /**
     * Sets the triNameTX value for this TaskType.
     * 
     * @param triNameTX
     */
    public void setTriNameTX(java.lang.String triNameTX) {
        this.triNameTX = triNameTX;
    }


    /**
     * Gets the triPlannedStartDT value for this TaskType.
     * 
     * @return triPlannedStartDT
     */
    public java.lang.String getTriPlannedStartDT() {
        return triPlannedStartDT;
    }


    /**
     * Sets the triPlannedStartDT value for this TaskType.
     * 
     * @param triPlannedStartDT
     */
    public void setTriPlannedStartDT(java.lang.String triPlannedStartDT) {
        this.triPlannedStartDT = triPlannedStartDT;
    }


    /**
     * Gets the triPlannedEndDT value for this TaskType.
     * 
     * @return triPlannedEndDT
     */
    public java.lang.String getTriPlannedEndDT() {
        return triPlannedEndDT;
    }


    /**
     * Sets the triPlannedEndDT value for this TaskType.
     * 
     * @param triPlannedEndDT
     */
    public void setTriPlannedEndDT(java.lang.String triPlannedEndDT) {
        this.triPlannedEndDT = triPlannedEndDT;
    }


    /**
     * Gets the triActualStartDT value for this TaskType.
     * 
     * @return triActualStartDT
     */
    public java.lang.String getTriActualStartDT() {
        return triActualStartDT;
    }


    /**
     * Sets the triActualStartDT value for this TaskType.
     * 
     * @param triActualStartDT
     */
    public void setTriActualStartDT(java.lang.String triActualStartDT) {
        this.triActualStartDT = triActualStartDT;
    }


    /**
     * Gets the triActualEndDT value for this TaskType.
     * 
     * @return triActualEndDT
     */
    public java.lang.String getTriActualEndDT() {
        return triActualEndDT;
    }


    /**
     * Sets the triActualEndDT value for this TaskType.
     * 
     * @param triActualEndDT
     */
    public void setTriActualEndDT(java.lang.String triActualEndDT) {
        this.triActualEndDT = triActualEndDT;
    }


    /**
     * Gets the comment value for this TaskType.
     * 
     * @return comment
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this TaskType.
     * 
     * @param comment
     */
    public void setComment(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] comment) {
        this.comment = comment;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType getComment(int i) {
        return this.comment[i];
    }

    public void setComment(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType _value) {
        this.comment[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TaskType)) return false;
        TaskType other = (TaskType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.triIdTX==null && other.getTriIdTX()==null) || 
             (this.triIdTX!=null &&
              this.triIdTX.equals(other.getTriIdTX()))) &&
            ((this.triNameTX==null && other.getTriNameTX()==null) || 
             (this.triNameTX!=null &&
              this.triNameTX.equals(other.getTriNameTX()))) &&
            ((this.triPlannedStartDT==null && other.getTriPlannedStartDT()==null) || 
             (this.triPlannedStartDT!=null &&
              this.triPlannedStartDT.equals(other.getTriPlannedStartDT()))) &&
            ((this.triPlannedEndDT==null && other.getTriPlannedEndDT()==null) || 
             (this.triPlannedEndDT!=null &&
              this.triPlannedEndDT.equals(other.getTriPlannedEndDT()))) &&
            ((this.triActualStartDT==null && other.getTriActualStartDT()==null) || 
             (this.triActualStartDT!=null &&
              this.triActualStartDT.equals(other.getTriActualStartDT()))) &&
            ((this.triActualEndDT==null && other.getTriActualEndDT()==null) || 
             (this.triActualEndDT!=null &&
              this.triActualEndDT.equals(other.getTriActualEndDT()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              java.util.Arrays.equals(this.comment, other.getComment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTriIdTX() != null) {
            _hashCode += getTriIdTX().hashCode();
        }
        if (getTriNameTX() != null) {
            _hashCode += getTriNameTX().hashCode();
        }
        if (getTriPlannedStartDT() != null) {
            _hashCode += getTriPlannedStartDT().hashCode();
        }
        if (getTriPlannedEndDT() != null) {
            _hashCode += getTriPlannedEndDT().hashCode();
        }
        if (getTriActualStartDT() != null) {
            _hashCode += getTriActualStartDT().hashCode();
        }
        if (getTriActualEndDT() != null) {
            _hashCode += getTriActualEndDT().hashCode();
        }
        if (getComment() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getComment());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getComment(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaskType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "TaskType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIdTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triIdTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triNameTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triNameTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triPlannedStartDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triPlannedStartDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triPlannedEndDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triPlannedEndDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triActualStartDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triActualStartDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triActualEndDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triActualEndDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "CommentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
