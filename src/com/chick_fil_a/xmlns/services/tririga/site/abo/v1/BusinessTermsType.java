/**
 * BusinessTermsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class BusinessTermsType  implements java.io.Serializable {
    private java.lang.String type;

    private java.lang.String acquisitionType;

    private java.math.BigDecimal purchasePrice;

    private java.math.BigDecimal annualRent;

    private java.lang.String termComments;

    private java.math.BigDecimal cstLeaseBuyoutNU;

    private java.math.BigDecimal cstCAMNU;

    private java.math.BigDecimal cstEscrowMoneyNU;

    public BusinessTermsType() {
    }

    public BusinessTermsType(
           java.lang.String type,
           java.lang.String acquisitionType,
           java.math.BigDecimal purchasePrice,
           java.math.BigDecimal annualRent,
           java.lang.String termComments,
           java.math.BigDecimal cstLeaseBuyoutNU,
           java.math.BigDecimal cstCAMNU,
           java.math.BigDecimal cstEscrowMoneyNU) {
           this.type = type;
           this.acquisitionType = acquisitionType;
           this.purchasePrice = purchasePrice;
           this.annualRent = annualRent;
           this.termComments = termComments;
           this.cstLeaseBuyoutNU = cstLeaseBuyoutNU;
           this.cstCAMNU = cstCAMNU;
           this.cstEscrowMoneyNU = cstEscrowMoneyNU;
    }


    /**
     * Gets the type value for this BusinessTermsType.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this BusinessTermsType.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the acquisitionType value for this BusinessTermsType.
     * 
     * @return acquisitionType
     */
    public java.lang.String getAcquisitionType() {
        return acquisitionType;
    }


    /**
     * Sets the acquisitionType value for this BusinessTermsType.
     * 
     * @param acquisitionType
     */
    public void setAcquisitionType(java.lang.String acquisitionType) {
        this.acquisitionType = acquisitionType;
    }


    /**
     * Gets the purchasePrice value for this BusinessTermsType.
     * 
     * @return purchasePrice
     */
    public java.math.BigDecimal getPurchasePrice() {
        return purchasePrice;
    }


    /**
     * Sets the purchasePrice value for this BusinessTermsType.
     * 
     * @param purchasePrice
     */
    public void setPurchasePrice(java.math.BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }


    /**
     * Gets the annualRent value for this BusinessTermsType.
     * 
     * @return annualRent
     */
    public java.math.BigDecimal getAnnualRent() {
        return annualRent;
    }


    /**
     * Sets the annualRent value for this BusinessTermsType.
     * 
     * @param annualRent
     */
    public void setAnnualRent(java.math.BigDecimal annualRent) {
        this.annualRent = annualRent;
    }


    /**
     * Gets the termComments value for this BusinessTermsType.
     * 
     * @return termComments
     */
    public java.lang.String getTermComments() {
        return termComments;
    }


    /**
     * Sets the termComments value for this BusinessTermsType.
     * 
     * @param termComments
     */
    public void setTermComments(java.lang.String termComments) {
        this.termComments = termComments;
    }


    /**
     * Gets the cstLeaseBuyoutNU value for this BusinessTermsType.
     * 
     * @return cstLeaseBuyoutNU
     */
    public java.math.BigDecimal getCstLeaseBuyoutNU() {
        return cstLeaseBuyoutNU;
    }


    /**
     * Sets the cstLeaseBuyoutNU value for this BusinessTermsType.
     * 
     * @param cstLeaseBuyoutNU
     */
    public void setCstLeaseBuyoutNU(java.math.BigDecimal cstLeaseBuyoutNU) {
        this.cstLeaseBuyoutNU = cstLeaseBuyoutNU;
    }


    /**
     * Gets the cstCAMNU value for this BusinessTermsType.
     * 
     * @return cstCAMNU
     */
    public java.math.BigDecimal getCstCAMNU() {
        return cstCAMNU;
    }


    /**
     * Sets the cstCAMNU value for this BusinessTermsType.
     * 
     * @param cstCAMNU
     */
    public void setCstCAMNU(java.math.BigDecimal cstCAMNU) {
        this.cstCAMNU = cstCAMNU;
    }


    /**
     * Gets the cstEscrowMoneyNU value for this BusinessTermsType.
     * 
     * @return cstEscrowMoneyNU
     */
    public java.math.BigDecimal getCstEscrowMoneyNU() {
        return cstEscrowMoneyNU;
    }


    /**
     * Sets the cstEscrowMoneyNU value for this BusinessTermsType.
     * 
     * @param cstEscrowMoneyNU
     */
    public void setCstEscrowMoneyNU(java.math.BigDecimal cstEscrowMoneyNU) {
        this.cstEscrowMoneyNU = cstEscrowMoneyNU;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BusinessTermsType)) return false;
        BusinessTermsType other = (BusinessTermsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.acquisitionType==null && other.getAcquisitionType()==null) || 
             (this.acquisitionType!=null &&
              this.acquisitionType.equals(other.getAcquisitionType()))) &&
            ((this.purchasePrice==null && other.getPurchasePrice()==null) || 
             (this.purchasePrice!=null &&
              this.purchasePrice.equals(other.getPurchasePrice()))) &&
            ((this.annualRent==null && other.getAnnualRent()==null) || 
             (this.annualRent!=null &&
              this.annualRent.equals(other.getAnnualRent()))) &&
            ((this.termComments==null && other.getTermComments()==null) || 
             (this.termComments!=null &&
              this.termComments.equals(other.getTermComments()))) &&
            ((this.cstLeaseBuyoutNU==null && other.getCstLeaseBuyoutNU()==null) || 
             (this.cstLeaseBuyoutNU!=null &&
              this.cstLeaseBuyoutNU.equals(other.getCstLeaseBuyoutNU()))) &&
            ((this.cstCAMNU==null && other.getCstCAMNU()==null) || 
             (this.cstCAMNU!=null &&
              this.cstCAMNU.equals(other.getCstCAMNU()))) &&
            ((this.cstEscrowMoneyNU==null && other.getCstEscrowMoneyNU()==null) || 
             (this.cstEscrowMoneyNU!=null &&
              this.cstEscrowMoneyNU.equals(other.getCstEscrowMoneyNU())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getAcquisitionType() != null) {
            _hashCode += getAcquisitionType().hashCode();
        }
        if (getPurchasePrice() != null) {
            _hashCode += getPurchasePrice().hashCode();
        }
        if (getAnnualRent() != null) {
            _hashCode += getAnnualRent().hashCode();
        }
        if (getTermComments() != null) {
            _hashCode += getTermComments().hashCode();
        }
        if (getCstLeaseBuyoutNU() != null) {
            _hashCode += getCstLeaseBuyoutNU().hashCode();
        }
        if (getCstCAMNU() != null) {
            _hashCode += getCstCAMNU().hashCode();
        }
        if (getCstEscrowMoneyNU() != null) {
            _hashCode += getCstEscrowMoneyNU().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BusinessTermsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "BusinessTermsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acquisitionType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "acquisitionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchasePrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "purchasePrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("annualRent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "annualRent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("termComments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "termComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstLeaseBuyoutNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstLeaseBuyoutNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstCAMNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstCAMNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstEscrowMoneyNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstEscrowMoneyNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
