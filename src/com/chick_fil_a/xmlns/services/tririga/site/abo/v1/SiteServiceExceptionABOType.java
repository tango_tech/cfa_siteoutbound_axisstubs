/**
 * SiteServiceExceptionABOType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class SiteServiceExceptionABOType  implements java.io.Serializable {
    private java.lang.String errorCodeTX;

    private java.lang.String errorDescriptionTX;

    private java.lang.String errorDetailsTX;

    private java.lang.String errorLocationTX;

    public SiteServiceExceptionABOType() {
    }

    public SiteServiceExceptionABOType(
           java.lang.String errorCodeTX,
           java.lang.String errorDescriptionTX,
           java.lang.String errorDetailsTX,
           java.lang.String errorLocationTX) {
           this.errorCodeTX = errorCodeTX;
           this.errorDescriptionTX = errorDescriptionTX;
           this.errorDetailsTX = errorDetailsTX;
           this.errorLocationTX = errorLocationTX;
    }


    /**
     * Gets the errorCodeTX value for this SiteServiceExceptionABOType.
     * 
     * @return errorCodeTX
     */
    public java.lang.String getErrorCodeTX() {
        return errorCodeTX;
    }


    /**
     * Sets the errorCodeTX value for this SiteServiceExceptionABOType.
     * 
     * @param errorCodeTX
     */
    public void setErrorCodeTX(java.lang.String errorCodeTX) {
        this.errorCodeTX = errorCodeTX;
    }


    /**
     * Gets the errorDescriptionTX value for this SiteServiceExceptionABOType.
     * 
     * @return errorDescriptionTX
     */
    public java.lang.String getErrorDescriptionTX() {
        return errorDescriptionTX;
    }


    /**
     * Sets the errorDescriptionTX value for this SiteServiceExceptionABOType.
     * 
     * @param errorDescriptionTX
     */
    public void setErrorDescriptionTX(java.lang.String errorDescriptionTX) {
        this.errorDescriptionTX = errorDescriptionTX;
    }


    /**
     * Gets the errorDetailsTX value for this SiteServiceExceptionABOType.
     * 
     * @return errorDetailsTX
     */
    public java.lang.String getErrorDetailsTX() {
        return errorDetailsTX;
    }


    /**
     * Sets the errorDetailsTX value for this SiteServiceExceptionABOType.
     * 
     * @param errorDetailsTX
     */
    public void setErrorDetailsTX(java.lang.String errorDetailsTX) {
        this.errorDetailsTX = errorDetailsTX;
    }


    /**
     * Gets the errorLocationTX value for this SiteServiceExceptionABOType.
     * 
     * @return errorLocationTX
     */
    public java.lang.String getErrorLocationTX() {
        return errorLocationTX;
    }


    /**
     * Sets the errorLocationTX value for this SiteServiceExceptionABOType.
     * 
     * @param errorLocationTX
     */
    public void setErrorLocationTX(java.lang.String errorLocationTX) {
        this.errorLocationTX = errorLocationTX;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SiteServiceExceptionABOType)) return false;
        SiteServiceExceptionABOType other = (SiteServiceExceptionABOType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errorCodeTX==null && other.getErrorCodeTX()==null) || 
             (this.errorCodeTX!=null &&
              this.errorCodeTX.equals(other.getErrorCodeTX()))) &&
            ((this.errorDescriptionTX==null && other.getErrorDescriptionTX()==null) || 
             (this.errorDescriptionTX!=null &&
              this.errorDescriptionTX.equals(other.getErrorDescriptionTX()))) &&
            ((this.errorDetailsTX==null && other.getErrorDetailsTX()==null) || 
             (this.errorDetailsTX!=null &&
              this.errorDetailsTX.equals(other.getErrorDetailsTX()))) &&
            ((this.errorLocationTX==null && other.getErrorLocationTX()==null) || 
             (this.errorLocationTX!=null &&
              this.errorLocationTX.equals(other.getErrorLocationTX())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrorCodeTX() != null) {
            _hashCode += getErrorCodeTX().hashCode();
        }
        if (getErrorDescriptionTX() != null) {
            _hashCode += getErrorDescriptionTX().hashCode();
        }
        if (getErrorDetailsTX() != null) {
            _hashCode += getErrorDetailsTX().hashCode();
        }
        if (getErrorLocationTX() != null) {
            _hashCode += getErrorLocationTX().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SiteServiceExceptionABOType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "SiteServiceExceptionABOType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCodeTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "errorCodeTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorDescriptionTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "errorDescriptionTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorDetailsTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "errorDetailsTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorLocationTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "errorLocationTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
