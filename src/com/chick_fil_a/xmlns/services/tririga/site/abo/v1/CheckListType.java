/**
 * CheckListType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class CheckListType  implements java.io.Serializable {
    private java.lang.String triIdTX;

    private java.lang.String triNameTX;

    private java.lang.String triTaskTypeCL;

    private java.lang.String cstChecklistDateDA;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType assignedTo;

    private java.lang.String triDescriptionTX;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] comment;

    public CheckListType() {
    }

    public CheckListType(
           java.lang.String triIdTX,
           java.lang.String triNameTX,
           java.lang.String triTaskTypeCL,
           java.lang.String cstChecklistDateDA,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType assignedTo,
           java.lang.String triDescriptionTX,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] comment) {
           this.triIdTX = triIdTX;
           this.triNameTX = triNameTX;
           this.triTaskTypeCL = triTaskTypeCL;
           this.cstChecklistDateDA = cstChecklistDateDA;
           this.assignedTo = assignedTo;
           this.triDescriptionTX = triDescriptionTX;
           this.comment = comment;
    }


    /**
     * Gets the triIdTX value for this CheckListType.
     * 
     * @return triIdTX
     */
    public java.lang.String getTriIdTX() {
        return triIdTX;
    }


    /**
     * Sets the triIdTX value for this CheckListType.
     * 
     * @param triIdTX
     */
    public void setTriIdTX(java.lang.String triIdTX) {
        this.triIdTX = triIdTX;
    }


    /**
     * Gets the triNameTX value for this CheckListType.
     * 
     * @return triNameTX
     */
    public java.lang.String getTriNameTX() {
        return triNameTX;
    }


    /**
     * Sets the triNameTX value for this CheckListType.
     * 
     * @param triNameTX
     */
    public void setTriNameTX(java.lang.String triNameTX) {
        this.triNameTX = triNameTX;
    }


    /**
     * Gets the triTaskTypeCL value for this CheckListType.
     * 
     * @return triTaskTypeCL
     */
    public java.lang.String getTriTaskTypeCL() {
        return triTaskTypeCL;
    }


    /**
     * Sets the triTaskTypeCL value for this CheckListType.
     * 
     * @param triTaskTypeCL
     */
    public void setTriTaskTypeCL(java.lang.String triTaskTypeCL) {
        this.triTaskTypeCL = triTaskTypeCL;
    }


    /**
     * Gets the cstChecklistDateDA value for this CheckListType.
     * 
     * @return cstChecklistDateDA
     */
    public java.lang.String getCstChecklistDateDA() {
        return cstChecklistDateDA;
    }


    /**
     * Sets the cstChecklistDateDA value for this CheckListType.
     * 
     * @param cstChecklistDateDA
     */
    public void setCstChecklistDateDA(java.lang.String cstChecklistDateDA) {
        this.cstChecklistDateDA = cstChecklistDateDA;
    }


    /**
     * Gets the assignedTo value for this CheckListType.
     * 
     * @return assignedTo
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType getAssignedTo() {
        return assignedTo;
    }


    /**
     * Sets the assignedTo value for this CheckListType.
     * 
     * @param assignedTo
     */
    public void setAssignedTo(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType assignedTo) {
        this.assignedTo = assignedTo;
    }


    /**
     * Gets the triDescriptionTX value for this CheckListType.
     * 
     * @return triDescriptionTX
     */
    public java.lang.String getTriDescriptionTX() {
        return triDescriptionTX;
    }


    /**
     * Sets the triDescriptionTX value for this CheckListType.
     * 
     * @param triDescriptionTX
     */
    public void setTriDescriptionTX(java.lang.String triDescriptionTX) {
        this.triDescriptionTX = triDescriptionTX;
    }


    /**
     * Gets the comment value for this CheckListType.
     * 
     * @return comment
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this CheckListType.
     * 
     * @param comment
     */
    public void setComment(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType[] comment) {
        this.comment = comment;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType getComment(int i) {
        return this.comment[i];
    }

    public void setComment(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.CommentType _value) {
        this.comment[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CheckListType)) return false;
        CheckListType other = (CheckListType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.triIdTX==null && other.getTriIdTX()==null) || 
             (this.triIdTX!=null &&
              this.triIdTX.equals(other.getTriIdTX()))) &&
            ((this.triNameTX==null && other.getTriNameTX()==null) || 
             (this.triNameTX!=null &&
              this.triNameTX.equals(other.getTriNameTX()))) &&
            ((this.triTaskTypeCL==null && other.getTriTaskTypeCL()==null) || 
             (this.triTaskTypeCL!=null &&
              this.triTaskTypeCL.equals(other.getTriTaskTypeCL()))) &&
            ((this.cstChecklistDateDA==null && other.getCstChecklistDateDA()==null) || 
             (this.cstChecklistDateDA!=null &&
              this.cstChecklistDateDA.equals(other.getCstChecklistDateDA()))) &&
            ((this.assignedTo==null && other.getAssignedTo()==null) || 
             (this.assignedTo!=null &&
              this.assignedTo.equals(other.getAssignedTo()))) &&
            ((this.triDescriptionTX==null && other.getTriDescriptionTX()==null) || 
             (this.triDescriptionTX!=null &&
              this.triDescriptionTX.equals(other.getTriDescriptionTX()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              java.util.Arrays.equals(this.comment, other.getComment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTriIdTX() != null) {
            _hashCode += getTriIdTX().hashCode();
        }
        if (getTriNameTX() != null) {
            _hashCode += getTriNameTX().hashCode();
        }
        if (getTriTaskTypeCL() != null) {
            _hashCode += getTriTaskTypeCL().hashCode();
        }
        if (getCstChecklistDateDA() != null) {
            _hashCode += getCstChecklistDateDA().hashCode();
        }
        if (getAssignedTo() != null) {
            _hashCode += getAssignedTo().hashCode();
        }
        if (getTriDescriptionTX() != null) {
            _hashCode += getTriDescriptionTX().hashCode();
        }
        if (getComment() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getComment());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getComment(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CheckListType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "CheckListType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIdTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triIdTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triNameTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triNameTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triTaskTypeCL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triTaskTypeCL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstChecklistDateDA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstChecklistDateDA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assignedTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "AssignedTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "PersonType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triDescriptionTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triDescriptionTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "CommentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
