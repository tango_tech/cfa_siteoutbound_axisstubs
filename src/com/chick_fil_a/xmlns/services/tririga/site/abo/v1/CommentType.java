/**
 * CommentType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class CommentType  implements java.io.Serializable {
    private int triRecordIdSY;

    private java.lang.String triCommentTX;

    private com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] commentedBy;

    public CommentType() {
    }

    public CommentType(
           int triRecordIdSY,
           java.lang.String triCommentTX,
           com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] commentedBy) {
           this.triRecordIdSY = triRecordIdSY;
           this.triCommentTX = triCommentTX;
           this.commentedBy = commentedBy;
    }


    /**
     * Gets the triRecordIdSY value for this CommentType.
     * 
     * @return triRecordIdSY
     */
    public int getTriRecordIdSY() {
        return triRecordIdSY;
    }


    /**
     * Sets the triRecordIdSY value for this CommentType.
     * 
     * @param triRecordIdSY
     */
    public void setTriRecordIdSY(int triRecordIdSY) {
        this.triRecordIdSY = triRecordIdSY;
    }


    /**
     * Gets the triCommentTX value for this CommentType.
     * 
     * @return triCommentTX
     */
    public java.lang.String getTriCommentTX() {
        return triCommentTX;
    }


    /**
     * Sets the triCommentTX value for this CommentType.
     * 
     * @param triCommentTX
     */
    public void setTriCommentTX(java.lang.String triCommentTX) {
        this.triCommentTX = triCommentTX;
    }


    /**
     * Gets the commentedBy value for this CommentType.
     * 
     * @return commentedBy
     */
    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] getCommentedBy() {
        return commentedBy;
    }


    /**
     * Sets the commentedBy value for this CommentType.
     * 
     * @param commentedBy
     */
    public void setCommentedBy(com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType[] commentedBy) {
        this.commentedBy = commentedBy;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType getCommentedBy(int i) {
        return this.commentedBy[i];
    }

    public void setCommentedBy(int i, com.chick_fil_a.xmlns.services.tririga.site.abo.v1.PersonType _value) {
        this.commentedBy[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CommentType)) return false;
        CommentType other = (CommentType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.triRecordIdSY == other.getTriRecordIdSY() &&
            ((this.triCommentTX==null && other.getTriCommentTX()==null) || 
             (this.triCommentTX!=null &&
              this.triCommentTX.equals(other.getTriCommentTX()))) &&
            ((this.commentedBy==null && other.getCommentedBy()==null) || 
             (this.commentedBy!=null &&
              java.util.Arrays.equals(this.commentedBy, other.getCommentedBy())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getTriRecordIdSY();
        if (getTriCommentTX() != null) {
            _hashCode += getTriCommentTX().hashCode();
        }
        if (getCommentedBy() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommentedBy());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommentedBy(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CommentType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "CommentType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triRecordIdSY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triRecordIdSY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triCommentTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "triCommentTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "commentedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "PersonType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
