/**
 * StoreType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.abo.v1;

public class StoreType  implements java.io.Serializable {
    private java.lang.String cstStoreNumberTX;

    private java.lang.String cstStoreNameTX;

    private java.math.BigDecimal cstSalesImpactLowNU;

    private java.math.BigDecimal cstSalesImpactHighNU;

    public StoreType() {
    }

    public StoreType(
           java.lang.String cstStoreNumberTX,
           java.lang.String cstStoreNameTX,
           java.math.BigDecimal cstSalesImpactLowNU,
           java.math.BigDecimal cstSalesImpactHighNU) {
           this.cstStoreNumberTX = cstStoreNumberTX;
           this.cstStoreNameTX = cstStoreNameTX;
           this.cstSalesImpactLowNU = cstSalesImpactLowNU;
           this.cstSalesImpactHighNU = cstSalesImpactHighNU;
    }


    /**
     * Gets the cstStoreNumberTX value for this StoreType.
     * 
     * @return cstStoreNumberTX
     */
    public java.lang.String getCstStoreNumberTX() {
        return cstStoreNumberTX;
    }


    /**
     * Sets the cstStoreNumberTX value for this StoreType.
     * 
     * @param cstStoreNumberTX
     */
    public void setCstStoreNumberTX(java.lang.String cstStoreNumberTX) {
        this.cstStoreNumberTX = cstStoreNumberTX;
    }


    /**
     * Gets the cstStoreNameTX value for this StoreType.
     * 
     * @return cstStoreNameTX
     */
    public java.lang.String getCstStoreNameTX() {
        return cstStoreNameTX;
    }


    /**
     * Sets the cstStoreNameTX value for this StoreType.
     * 
     * @param cstStoreNameTX
     */
    public void setCstStoreNameTX(java.lang.String cstStoreNameTX) {
        this.cstStoreNameTX = cstStoreNameTX;
    }


    /**
     * Gets the cstSalesImpactLowNU value for this StoreType.
     * 
     * @return cstSalesImpactLowNU
     */
    public java.math.BigDecimal getCstSalesImpactLowNU() {
        return cstSalesImpactLowNU;
    }


    /**
     * Sets the cstSalesImpactLowNU value for this StoreType.
     * 
     * @param cstSalesImpactLowNU
     */
    public void setCstSalesImpactLowNU(java.math.BigDecimal cstSalesImpactLowNU) {
        this.cstSalesImpactLowNU = cstSalesImpactLowNU;
    }


    /**
     * Gets the cstSalesImpactHighNU value for this StoreType.
     * 
     * @return cstSalesImpactHighNU
     */
    public java.math.BigDecimal getCstSalesImpactHighNU() {
        return cstSalesImpactHighNU;
    }


    /**
     * Sets the cstSalesImpactHighNU value for this StoreType.
     * 
     * @param cstSalesImpactHighNU
     */
    public void setCstSalesImpactHighNU(java.math.BigDecimal cstSalesImpactHighNU) {
        this.cstSalesImpactHighNU = cstSalesImpactHighNU;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StoreType)) return false;
        StoreType other = (StoreType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cstStoreNumberTX==null && other.getCstStoreNumberTX()==null) || 
             (this.cstStoreNumberTX!=null &&
              this.cstStoreNumberTX.equals(other.getCstStoreNumberTX()))) &&
            ((this.cstStoreNameTX==null && other.getCstStoreNameTX()==null) || 
             (this.cstStoreNameTX!=null &&
              this.cstStoreNameTX.equals(other.getCstStoreNameTX()))) &&
            ((this.cstSalesImpactLowNU==null && other.getCstSalesImpactLowNU()==null) || 
             (this.cstSalesImpactLowNU!=null &&
              this.cstSalesImpactLowNU.equals(other.getCstSalesImpactLowNU()))) &&
            ((this.cstSalesImpactHighNU==null && other.getCstSalesImpactHighNU()==null) || 
             (this.cstSalesImpactHighNU!=null &&
              this.cstSalesImpactHighNU.equals(other.getCstSalesImpactHighNU())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCstStoreNumberTX() != null) {
            _hashCode += getCstStoreNumberTX().hashCode();
        }
        if (getCstStoreNameTX() != null) {
            _hashCode += getCstStoreNameTX().hashCode();
        }
        if (getCstSalesImpactLowNU() != null) {
            _hashCode += getCstSalesImpactLowNU().hashCode();
        }
        if (getCstSalesImpactHighNU() != null) {
            _hashCode += getCstSalesImpactHighNU().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StoreType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "StoreType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstStoreNumberTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstStoreNumberTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstStoreNameTX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstStoreNameTX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstSalesImpactLowNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstSalesImpactLowNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cstSalesImpactHighNU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/abo/v1", "cstSalesImpactHighNU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
