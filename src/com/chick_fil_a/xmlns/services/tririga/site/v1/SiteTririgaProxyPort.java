/**
 * SiteTririgaProxyPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.v1;

public interface SiteTririgaProxyPort extends java.rmi.Remote {
    public com.chick_fil_a.xmlns.services.tririga.site.abm.v1.SyncSiteResponseABM syncSite(com.chick_fil_a.xmlns.services.tririga.site.abm.v1.SyncSiteRequestABM parameters) throws java.rmi.RemoteException;
}
