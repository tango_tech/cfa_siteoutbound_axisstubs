/**
 * SiteTririgaProxy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.v1;

public interface SiteTririgaProxy extends javax.xml.rpc.Service {
    public java.lang.String getSiteTririgaProxyServiceSOAPAddress();

    public com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyPort getSiteTririgaProxyServiceSOAP() throws javax.xml.rpc.ServiceException;

    public com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyPort getSiteTririgaProxyServiceSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
