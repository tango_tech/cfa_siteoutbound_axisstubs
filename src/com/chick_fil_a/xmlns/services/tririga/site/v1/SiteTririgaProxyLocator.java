/**
 * SiteTririgaProxyLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.chick_fil_a.xmlns.services.tririga.site.v1;

public class SiteTririgaProxyLocator extends org.apache.axis.client.Service implements com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxy {

    public SiteTririgaProxyLocator() {
    }


    public SiteTririgaProxyLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SiteTririgaProxyLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SiteTririgaProxyServiceSOAP
    private java.lang.String SiteTririgaProxyServiceSOAP_address = "https://localhost:8888/iwms/SiteTririgaService/ProxyServices/SiteTririgaProxy";

    public java.lang.String getSiteTririgaProxyServiceSOAPAddress() {
        return SiteTririgaProxyServiceSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SiteTririgaProxyServiceSOAPWSDDServiceName = "SiteTririgaProxyServiceSOAP";

    public java.lang.String getSiteTririgaProxyServiceSOAPWSDDServiceName() {
        return SiteTririgaProxyServiceSOAPWSDDServiceName;
    }

    public void setSiteTririgaProxyServiceSOAPWSDDServiceName(java.lang.String name) {
        SiteTririgaProxyServiceSOAPWSDDServiceName = name;
    }

    public com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyPort getSiteTririgaProxyServiceSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SiteTririgaProxyServiceSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSiteTririgaProxyServiceSOAP(endpoint);
    }

    public com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyPort getSiteTririgaProxyServiceSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyServiceSOAPStub _stub = new com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyServiceSOAPStub(portAddress, this);
            _stub.setPortName(getSiteTririgaProxyServiceSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSiteTririgaProxyServiceSOAPEndpointAddress(java.lang.String address) {
        SiteTririgaProxyServiceSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyPort.class.isAssignableFrom(serviceEndpointInterface)) {
                com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyServiceSOAPStub _stub = new com.chick_fil_a.xmlns.services.tririga.site.v1.SiteTririgaProxyServiceSOAPStub(new java.net.URL(SiteTririgaProxyServiceSOAP_address), this);
                _stub.setPortName(getSiteTririgaProxyServiceSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SiteTririgaProxyServiceSOAP".equals(inputPortName)) {
            return getSiteTririgaProxyServiceSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/v1", "SiteTririgaProxy");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://xmlns.chick-fil-a.com/services/tririga/site/v1", "SiteTririgaProxyServiceSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SiteTririgaProxyServiceSOAP".equals(portName)) {
            setSiteTririgaProxyServiceSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
